const Db = require('./models/Db')

class Auth {
    Db
    constructor() {
        this.Db = new Db('wc_license')
    }

    amzAuth = async (req, res, next) => {
        var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
        if (ip.match(/::ffff:/i)) {
            ip = ip.replace(/::ffff:/i, "")
        }
        let find = await this.Db.readOne({ ip: ip }, 'amz_lic')
        if (find) {
            return next()
        }
        res.json({ code: 401 })
    }
}

module.exports = new Auth()