const MongoClient = require("mongodb").MongoClient;

class Db {
  uri;
  MongoClient;
  client;
  schema;
  db;
  collection;
  target;
  constructor(target) {
    this.MongoClient = MongoClient;
    this.uri =
      "mongodb+srv://Admur04:Admur04012000@cluster0.dya7k.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    this.client = new this.MongoClient(this.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    this.target = target
    this.client.connect((err) => {
      if (err) return console.log("failed to connect to db -> " + err);
      this.dbprepare()
      return console.log("Connected to database -> " + this.target);
    });
  }

  dbprepare = () => {
    this.db = this.client.db(this.target);
    this.schema = {};
  }

  Schema(schema) {
    this.schema = schema;
  }

  Col(collection) {
    this.collection = this.db.collection(collection)
  }

  readOne = async (schema, collection) => {
    this.Schema(schema)
    this.Col(collection)
    let data = await this.collection.findOne(this.schema)
    return data
  }
}

module.exports = Db;
